const { authenticate } = require('@feathersjs/authentication').hooks;

const search = require('feathers-nedb-fuzzy-search')

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

const logger = require('../../logger');

const logNewUserCreated = () => {
  return (context) => {
    logger.info(`New user with _id : ${context.result._id} created !`); 
  }
}


const updatePostFollowing = require('../../hooks/update-post-following');


module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt'),  search({fields: ["email", "name"]})],
    get: [ authenticate('jwt') ],
    create: [ hashPassword('password') ],
    update: [ hashPassword('password'),  authenticate('jwt') ],
    patch: [hashPassword('password'), authenticate('jwt')],
    remove: [ authenticate('jwt') ]
  },

  after: {
    all: [ 
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [logNewUserCreated()],
    update: [],
    patch: [updatePostFollowing()],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
