const { Service } = require('feathers-nedb');
const logger = require('../../logger'); 
const crypto = require('crypto'); 

const gravatarUrl = 'https://gravatar.com/avatar';
const query = "d=identicon&s=80";

const getGravatar = email => {
    const hash = crypto.createHash('md5').update(email.toLowerCase()).digest('hex');
    return `${gravatarUrl}/${hash}?${query}`
};


exports.Users = class Users extends Service {
    async create(data, params){
        logger.info("Starting create from User Service...."); 
        const { email, password, googleId, name } = data;
        const avatar = data.avatar || getGravatar(email); 
        const userData = {
            email,
            name,
            password,
            googleId,
            avatar,
            follows: [],
            followers: []
        };

        return super.create(userData, params); 
    }
};
