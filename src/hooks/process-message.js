// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { data, app } = context;
    
    if(!data.text){
      throw new Error("A message must have a text field");
    }

    const text = data.text.slice(0, 400); 

    const { user } = context.params;
    const { receiverId } = data;
    
    if(receiverId){
      app.service('users').get(receiverId, context.params, (user) => {
        if(!user){
          throw new Error("The receiver does not exist, aborted");
        }
      });
    }

    context.data = {
      senderId: user._id, 
      receiverId,
      sentAt: new Date(),
      text
    };

    return context;
  };
};
